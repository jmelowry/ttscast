#!/usr/bin/env python

import re
from bs4 import BeautifulSoup
import requests

# basic structure:
# articles are outputted as lists. Each paragraph, a new list item.
# class parses based on popular news sites

#print(soup.prettify())

class ExtractText:

    def __init__(self, article_url):
        self.article_url = article_url
        self.article_title = ""
        self.article_byline = ""
        self.article_date = ""
        self.article_body = []
        self.html = requests.get(article_url).text
        self.soup = BeautifulSoup(self.html, 'lxml')
        self.get_article_text()

    def get_article_text(self):

        # define the div class being used by publication
        div_classes = {
            "nytimes.com": ["StoryBodyCompanionColumn"],
            "cnn.com": ["zn-body__paragraph speakable", "zn-body__paragraph"],
            "vox.com": "c-entry-content",
            "verge.com": "c-entry-content"
        }

        for k, v in div_classes.items():
            #print(k,v)
            if k in self.article_url:
                # get heading (cnn/ nytimes)
                self.article_title = self.soup.find('h1').text
                if 'nytimes.com' in k:
                    self.article_byline = self.soup.find('meta',attrs={'name':'byl'})['content']
                    self.article_date = self.soup.find('meta',attrs={'property':'article:published'})['content']

                    #<meta data-rh="true" property="article:published" itemprop="datePublished" content="2018-07-25T20:52:20.000Z"/>
                elif 'cnn.com' in k:
                    self.article_byline = self.soup.find(class_='metadata__byline__author').text
                    self.article_date = self.soup.find('meta',attrs={'name':'pubdate'})['content']

                self.div_class = v

                for i in self.soup.find_all(class_=self.div_class):
                    self.article_body.append((i.text))

if __name__ == '__main__':

    cnn_test = ExtractText('https://www.cnn.com/2018/07/25/politics/donald-trump-michael-cohen-tape-recording/index.html')
    print(cnn_test.article_title)
    print(cnn_test.article_date)
    print(cnn_test.article_byline)
    #for paragraph in cnn_test.article_body:
    #    print(paragraph)

    nytimes_test = ExtractText('https://www.nytimes.com/2018/07/24/us/politics/trump-cohen-tape.html')
    print(nytimes_test.article_title)
    print(nytimes_test.article_date)
    print(nytimes_test.article_byline)
