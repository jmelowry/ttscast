#!/usr/bin/env python

from gtts import gTTS
import os

tts = gTTS(text='Good morning', lang='en')

class Tts:

    def __init__(self, input_files):
        self.input_files = input_files
        self.process_input_files()

    def write_audio_file(self, text, filename):
        tts = gTTS(text=text, lang='en')
        tts.save('output/' + filename.split('.')[0] + '.mp3')

    def process_input_files(self):
        for file in self.input_files:
            print(file)
            with open(file, 'r') as text_file:
                text = text_file.read()
            self.write_audio_file(text, file)


if __name__ == '__main__':
    compilation = Tts(['sample.txt'])
